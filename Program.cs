﻿using System;

namespace SingletonProj
{
    class Program
    {
        static void Main(string[] args)
        {

            
            SingletonProj singletonProjekt = SingletonProj.Instance;
            singletonProjekt.x = 1;
            

            SingletonProj singletonProjekt2 = SingletonProj.Instance;
            singletonProjekt2.x = 3;

            Console.WriteLine("If this: ");
            singletonProjekt.SingletonMain();
            Console.WriteLine("is the same as this: ");
            singletonProjekt2.SingletonMain();
            Console.WriteLine("then Singleton works fine");
            
        }

        
    }
    
    public class SingletonProj
    {
        private static SingletonProj instance = null;
        public int x { get; set; }
        public static SingletonProj Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new SingletonProj();
                }
                return instance; }
            }
        
        public void SingletonMain()
        {
            Math(x);
            
        }

        private SingletonProj() { }

        static void Math(int x)
        {
            int n = 2 + x;
            Console.WriteLine("2 + x = " +n);
        }

    }

    
}
